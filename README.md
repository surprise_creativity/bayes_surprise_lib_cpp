# BayesSurprise : Biblioteca de Surpresa Bayesiana #

*Implementação de Surpresa Bayesiana para aplicações de Criatividade Computacional baseado em [Baldi, P. e Itti, L (2010)](http://ilab.usc.edu/publications/doc/Baldi_Itti10nn.pdf).*

### Conteúdo deste repositório: ###

* Projeto na [IDE Eclipse for C/C++ Developers](http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/heliossr2) em linguagem de programação C++ (compilador GCC/G++).

### Dependência: ###

* **[GSL - GNU Scientific Library](https://www.gnu.org/software/gsl/)**:
    * Biblioteca de Software Livre em linguagem de programação C++;
    * Biblioteca de cálculo numérico;
    * Distribuida sobre a GNU General Public License (o que afeta a licensa de redistribuição desta biblioteca - BayesSurprise for C++ Lib).

### Como configurar o repositório no Linux (Ubuntu): ###

1. Clonar o repositório:
    * Abra o console de comando do sistema operacional e navegue até o local onde será clonado o repositório;
    * Abra a pagina de [`Overview`](https://bitbucket.org/surprise_creativity/bayes_surprise_lib_cpp/overview) deste repositório, copie o link HTTPS no topo da pagina e execute os comandos abaixo;
    * `git clone link_HTTPS_copiado BayesSurprise`;
    * Digite sua senha do Bitbucket quando exigido e `Enter`.
2. Instalar e adicionar a **GSL Library** ao projeto:
    * O projeto já vem com a biblioteca adicionada, necessitando apenas a sua instalação no sistema operacional (passos abaixo); 
    * Com o console aberto, navegue até alguma pasta de sua preferência para baixar a biblioteca;
    * Em um navegador, visite a pagina [oficial do projeto da GSL](https://www.gnu.org/software/gsl/), vá até a seção `Downloading GSL` e clique no link [`gnu mirror`](http://ftpmirror.gnu.org/gsl/);
    * Na pagina de [`gnu mirror`](http://ftpmirror.gnu.org/gsl/) do projeto GSL, escolha um arquivo `.tar.gz` de versão igual ou superior à `2.3` (e.g. `gsl-2.3.tar.gz`) e copie o link deste arquivo (e.g. `http://mirror.nbtelecom.com.br/gnu/gsl/gsl-2.3.tar.gz`);
    * Novamente no console, execute os comandos abaixo;
    * `wget -c link_copiado_gsl`;
    * `tar -vzxf gsl-*`;
    * `rm *.tar.gz`;
    * `cd gsl-*`;
    * `./configure`;
    * `make`;
    * `make install`;
    * `cd ..`;
    * `rm -r gsl-*`.
3. Importar o projeto na IDE Eclipse:
    * No menu `File`, clique no sub-menu `Import`;
    * Na janela de seleção, dentro da pasta `General`, dê um duplo-clique na opção `Projects from Folder or Archive`;
    * Na janela de seleção da fonte de importação, clique em `Diretory...` e selecione no seu sistemas de arquivo a pasta onde foi clonado o repositório (i.e. a pasta `BayesSurprise`).
    * Mantenha os dois checkbox, `Search for nested projects` e `Detect and configure project natures`, marcados e pressione o botão `Finish`.

### Como utilizar a biblioteca: ###

No arquivo `BayesSurprise/src/Exemplo.cpp` contém todos os detalhes de utilização da biblioteca. Abaixo segue alguns exemplos básicos dentre os contidos nesse arquivo.

* Organização dos pacotes:
    * O pacote raiz é o `surp`;
    * O pacote `surp/artifact` contém a abstração de um artefato de Criatividade Computacional;
    * O pacote `surp/evaluation` contém classes e interfaces para avaliação da surpresa de artefatos;
    * O pacote `surp/model` contém classes e interfaces para abstração de modelos de Surpresa Bayesiana;
    * O pacote `surp/model/distribuition` contém classes que implementam os modelos de Surpresa Bayesiana para diferentes tipos de dados;
    * Adicionalmente existe o pacote `stream` com classes e interfaces para tratamento de entrada e saída de dados utilizado pela biblioteca.
* Criando um artefato de Criatividade Computacional:
```c++
  #include "surp/artifact/ObjectArtifact.h"
  
  //...
  
  art::ObjectArtifact * objArt = new art::ObjectArtifact();
  // Adicionando valores de atributos ao artefato:
  objArt->add(true); // Booleanos
  objArt->add(2);    // Inteiros maiores que 0
  objArt->add(2.0);  // Reais
  //...
  objArt->add(5.0);

  //...

  delete objArt;
```
* Criando um modelo para avaliação de Surpresa Bayesiana:
```c++
  // Modelo para artefato:
  #include "surp/model/ObjectModel.h"
  // Modelos para atributos:
  // Booleano
  #include "surp/model/distribution/BetaDistribution.h"
  // Inteiro (maior que 0)
  #include "surp/model/distribution/PoissonDistribution.h"
  // Real (variância fixa)
  #include "surp/model/distribution/GaussianDistribution.h"
  // Real (média fixa)
  #include "surp/model/distribution/InvGammaDistribution.h"
  // Real (livre)
  #include "surp/model/distribution/GssInvGammaDistribution.h"

  //...
  
  mdl::ObjectModel * objMdl = new mdl::ObjectModel();

  // Booleano com média a priori de 50% true
  dist::BetaDistribution * boolDist = new dist::BetaDistribution(0.5);
  // Inteiro com média a priori de 2.0
  dist::PoissonDistribution * intDist = 
                                   new dist::PoissonDistribution(2.0);
  // Real com variância fixa em 2.0
  dist::GaussianDistribution * doubleDistFixVar = 
                                   new dist::GaussianDistribution(2.0);
  // Real com média fixa em 3.0
  dist::InvGammaDistribution * doubleDistFixMean = 
                                   new dist::InvGammaDistribution(3.0);
  // Real com média e variância livres
  dist::GssInvGammaDistribution * doubleDist = 
                                   new dist::GssInvGammaDistribution();
  // Obs.: Cada atributo deve ter seu próprio objeto de distribuição,
  //       mesmo que seja do mesmo tipo de dado.
		
  // Adição dos modelos Bayesianos ao modelo de artefato
  // (mesma ordem dos tipos de atributos nos artefatos): 
  objMdl->add(boolDist);
  objMdl->add(intDist);
  objMdl->add(doubleDistFixVar);
  //...
  objMdl->add(doubleDist);

  //...

  // Gravando os DADOS do modelo em arquivo:
  objMdl->write("modelos.dat");
  
  //...

  // Lendo os DADOS do modelo a partir de arquivo:
  objMdl->read("modelos.dat");
  // Obs.: a escrita não persiste o número e tipo dos atributos. Dessa
  //       forma, antes de usar 'read' o objeto de modelo, no caso
  //       'objMdl', já deve ter sido criado e inicializado com
  //       distribuições dos tipos corretos.

  //...

  delete objMdl;
  
  delete boolDist;
  delete intDist;
  delete doubleDistFixVar;
  delete doubleDistFixMean;
  delete doubleDist;
```
* Treinando e avaliando os artefatos através dos modelos:
```c++
  // Classe estática de avaliação
  #include "surp/evaluation/ArtifactEval.h"
  // Compõe a surpresa dos atributos em um único valor
  #include "surp/evaluation/SimpleIntegrater.h"
  // Obs.: pode ser implementado um agregador mais complexo que 
  //       'SimpleIntegrater' via interface 'surp/evaluation/Integrater'.

  // ...

  // Agregador que apenas soma a surpresa dos atributos:
  evl::SimpleIntegrater * itg = new evl::SimpleIntegrater();

  // Avaliando a compatibilidade dos tipos entre o modelo e o artefato:
  if(evl::isCompatible(objMdl, objArt)){
     std::cout << "\n\nFormato compatível!\n\n" << std::endl;
  }

  //...

  // Treinando o modelo através de um artefato:
  evl::learn(objMdl, objArt);
  // Obs.: os artefatos não precisam ser persistidos num 'data set', 
  //       porque os modelos são iterativos. Dessa forma, uma vez 
  //       usado 'learn' o modelo apreendeu o artefato.

  //...

  // Avaliando a surpresa de um artefato:
  double surprise = evl::getSurprise(objMdl, objArt, itg);
  std::cout << "Surpresa do artefato: " << surprise << "\n" << std::endl;

  //...

  delete itg;
```

### Contato: ###

Para perguntas, sugestões ou reportar erros nos contate por Email:

* Willian A.S. (Will1Dexter@hotmail.com)

### Licença: ###

No arquivo `BayesSurprise/License.txt` e no topo de cada código fonte em `BayesSurprise/src/` contém um banner, que não pode ser removido, com informações sobre a licença de uso desta biblioteca (válida apenas para uso interno - "in-house" - distribuir, por causa da GSL, deve ser pela licença GNU General Public License).

Fique a vontade para nos contatar ou contribuir com este projeto.