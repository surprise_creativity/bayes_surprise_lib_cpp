/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: Exemplo.cpp   Exemplos de uso da lib 'surp' de Surpresa      */
/*                                     Bayesiana.                                    */
/* --------------------------------------------------------------------------------- */

#include <iostream>
#include "surp/artifact/ObjectArtifact.h"
#include "surp/evaluation/ArtifactEval.h"
#include "surp/evaluation/SimpleIntegrater.h"
#include "surp/model/ObjectModel.h"
#include "surp/model/distribution/BetaDistribution.h"
#include "surp/model/distribution/PoissonDistribution.h"
#include "surp/model/distribution/GaussianDistribution.h"
#include "surp/model/distribution/InvGammaDistribution.h"
#include "surp/model/distribution/GssInvGammaDistribution.h"

/**
 * Exemplo de uso da biblioteca de surpresa bayesiana.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

// Flag de pausa da aplicação ao término:
#define RUN_PAUSE false

int main(int argc, char **argv) {
	// Criação dos artefatos:
	art::ObjectArtifact * objArt1 = new art::ObjectArtifact();
	objArt1->add(true);
	objArt1->add(2);   // > 0
	objArt1->add(2.0); // var  = 2
	objArt1->add(5.0); // mean = 3
	objArt1->add(-2.0);

	art::ObjectArtifact * objArt2 = new art::ObjectArtifact();
	objArt2->add(true);
	objArt2->add(3);   // > 0
	objArt2->add(1.0); // var  = 2
	objArt2->add(2.0); // mean = 3
	objArt2->add(-1.0);

	art::ObjectArtifact * objArt3 = new art::ObjectArtifact();
	objArt3->add(false);
	objArt3->add(1);   // > 0
	objArt3->add(2.5); // var  = 2
	objArt3->add(6.0); // mean = 3
	objArt3->add(8.0);

	art::ObjectArtifact * objArt4 = new art::ObjectArtifact();
	objArt4->add(false);
	objArt4->add(5);   // > 0
	objArt4->add(2.0); // var  = 2
	objArt4->add(3.0); // mean = 3
	objArt4->add(-5.0);

	art::ObjectArtifact * objArt5 = new art::ObjectArtifact();
	objArt5->add(true);
	objArt5->add(6);    // > 0
	objArt5->add(-0.5); // var  = 2
	objArt5->add(4.0);  // mean = 3
	objArt5->add(10.0);

	// Criação do objeto para conjunto de modelos bayesianos:
	mdl::ObjectModel * objMdl = new mdl::ObjectModel();

	// Criação de modelos bayesianos para cada tipo de distribuição:
	dist::BetaDistribution * boolDist = new dist::BetaDistribution(0.5);                   // Dado booleano. Neste exemplo com média a priori de 0.5 (igual uma moeda não viciada).
	dist::PoissonDistribution * intDist = new dist::PoissonDistribution(2.0);              // Dado inteiro (maior que 0). Neste exemplo com média a priori de 2.0.
	dist::GaussianDistribution * doubleDistFixVar = new dist::GaussianDistribution(2.0);   // Dado real com apenas a variancia fixa em 2.
	dist::InvGammaDistribution * doubleDistFixMean = new dist::InvGammaDistribution(3.0);  // Dado real com apenas a média fixa em 3.
	dist::GssInvGammaDistribution * doubleDistNoFix = new dist::GssInvGammaDistribution(); // Dado real sem média ou variância fixa.

	// Adição dos modelos bayesianos ao objeto de conjunto de modelos:
	objMdl->add(boolDist);
	objMdl->add(intDist);           // > 0
	objMdl->add(doubleDistFixVar);  // var  = 2
	objMdl->add(doubleDistFixMean); // mean = 3
	objMdl->add(doubleDistNoFix);

	// Obs: a ordem dos tipos de dados deve ser a mesma dos artefatos.

	// Agregador das surpresas das características do artefato:
	evl::SimpleIntegrater * itg = new evl::SimpleIntegrater(); // Apenas soma a surpresa.

	// Utilizando o método estático isCompatible da classe ArtifactEval, avalia compatibilidade dos tipos entre o conjunto de modelos e os artefatos:
	if(evl::isCompatible(objMdl, objArt1) && evl::isCompatible(objMdl, objArt2) && evl::isCompatible(objMdl, objArt3)  && evl::isCompatible(objMdl, objArt4) && evl::isCompatible(objMdl, objArt5)){
		std::cout << "\n\nFormato compatível do modelo em relação aos artefatos!\n\n" << std::endl;
	}

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	// Gravação dos parâmetros dos modelos bayesianos no conjunto de modelos:

	std::cout << "Gravando modelos iniciais!\n\n" << std::endl;

	objMdl->write("modelos.dat");

	// Obs: a escrita e leitura correta (em termos de órdem de modelos bayesianos) é responsabilidade do usuário da biblioteca.
	//      Isto é garantido pela utilização de conjunto de modelos com mesmas distribuições entre escrita e leitura.

	// Testes de avaliação de surpresa e treinamento:

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apenas modelos a priori (i.e. sem apreender nenhum artefato)!\n\n" << std::endl;

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apreendendo o artefato 1!\n\n" << std::endl;

	evl::learn(objMdl, objArt1);

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apreendendo o artefato 2!\n\n" << std::endl;

	evl::learn(objMdl, objArt2);

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apreendendo o artefato 3!\n\n" << std::endl;

	evl::learn(objMdl, objArt3);

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apreendendo o artefato 4!\n\n" << std::endl;

	evl::learn(objMdl, objArt4);

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	std::cout << "Apreendendo o artefato 5!\n\n" << std::endl;

	evl::learn(objMdl, objArt5);

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	std::cout << "----------------------------------------------------------------\n\n" << std::endl;

	// Leitura dos parâmetros dos modelos bayesianos do conjunto de modelos:

	std::cout << "Lendo modelos iniciais (ignorando o treinamento)!\n\n" << std::endl;

	objMdl->read("modelos.dat");

	// Obs: a escrita e leitura correta (em termos de órdem de modelos bayesianos) é responsabilidade do usuário da biblioteca.
	//      Isto é garantido pela utilização de conjunto de modelos com mesmas distribuições entre escrita e leitura.

	std::cout << "Surpresa do artefato 1: " << evl::getSurprise(objMdl, objArt1, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 2: " << evl::getSurprise(objMdl, objArt2, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 3: " << evl::getSurprise(objMdl, objArt3, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 4: " << evl::getSurprise(objMdl, objArt4, itg) << "\n" << std::endl;

	std::cout << "Surpresa do artefato 5: " << evl::getSurprise(objMdl, objArt5, itg) << "\n\n" << std::endl;

	delete objArt1;
	delete objArt2;
	delete objArt3;
	delete objArt4;
	delete objArt5;

	delete objMdl;

	delete boolDist;
	delete intDist;
	delete doubleDistFixVar;
	delete doubleDistFixMean;
	delete doubleDistNoFix;

	delete itg;

	if(RUN_PAUSE){
		std::cout << "\n\nDigite qualquer coisa e enter para sair..." << std::endl;
		char aux[100];
		std::cin >> aux;
	}
	return 0;
}
