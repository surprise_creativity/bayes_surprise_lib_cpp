/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BufferedInputStream.cpp   Entrada de fluxo de dados bufferi- */
/*                                                 zada.                             */
/* --------------------------------------------------------------------------------- */

#include "BufferedInputStream.h"
#include "BufferedOutputStream.h"
#include <cstdio>

namespace strm {

/**
 * Construtor com o fluxo de entrada de dados passado via parâmetro (tamanho máximo do buffer padrão).
 * @param _in Fluxo de entrada de dados.
 */
BufferedInputStream::BufferedInputStream(InputStream* _in) {
	size = DEFAULT_SIZE_BUFF;
	pos_out = 0;
	quant = 0;
	in = _in;

	for(int i = 0;i < size;i++){
		buffer.push_back(0);
	}
}

/**
 * Construtor com o fluxo de entrada de dados e tamanho máximo do buffer passados via parâmetro.
 * @param _in Fluxo de entrada de dados.
 * @param _size Tamanho máximo do buffer.
 */
BufferedInputStream::BufferedInputStream(InputStream* _in, int _size) {
	if(_size <= 0){
		_size = DEFAULT_SIZE_BUFF;
	}
	size = _size;
	pos_out = 0;
	quant = 0;
	in = _in;

	for(int i = 0;i < size;i++){
		buffer.push_back(0);
	}
}

BufferedInputStream::~BufferedInputStream() {
	close();
}

/**
 * Lê 'len' booleanos e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os booleanos lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(bool* values, int offset, int len) {
	int result = 0;
	int len_index = (offset + len);
	for(int i = offset;i < len_index;i++){
		if(read((&values[i]))){
			result++;
		}else{
			i = len_index;
		}
	}
	return result;
}

/**
 * Lê um booleano e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o booleano lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(bool* value) {
	bool result = false;
	char byte_value;
	if(read(&byte_value)){
		*value = (byte_value == 1);
		result = true;
	}
	return result;
}

/**
 * Lê 'len' bytes(char) e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os bytes(char) lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(char* values, int offset, int len) {
	int result = 0;
	int len_index = (offset + len);
	for(int i = offset;i < len_index;i++){
		if(read((&values[i]))){
			result++;
		}else{
			i = len_index;
		}
	}
	return result;
}

/**
 * Lê um byte(char) e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o byte(char) lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(char* value) {
	bool result = true;
	if(quant == 0){
		result = load();
	}

	if(result){
		if(pos_out < (int)buffer.size()){
			*value = buffer.at(pos_out);
			pos_out++;
			quant--;
		}else{
			pos_out = 0;
			*value = buffer.at(pos_out);
			pos_out++;
			quant--;
		}
	}

	return result;
}

/**
 * Lê 'len' short's e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os short's lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(short * values, int offset, int len) {
	return read_generic<short, short_to_byte>(values, offset, len);
}

/**
 * Lê um short e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o short lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(short * value) {
	return read_generic<short, short_to_byte>(value);
}

/**
 * Lê 'len' int's e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os int's lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(int* values, int offset, int len) {
	return read_generic<int, int_to_byte>(values, offset, len);
}

/**
 * Lê um int e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o int lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(int* value) {
	return read_generic<int, int_to_byte>(value);
}

/**
 * Lê 'len' long's e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os long's lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(long * values, int offset, int len) {
	return read_generic<long, long_to_byte>(values, offset, len);
}

/**
 * Lê um long e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o long lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(long * value) {
	return read_generic<long, long_to_byte>(value);
}

/**
 * Lê 'len' float's e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os float's lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(float* values, int offset, int len) {
	return read_generic<float, float_to_byte>(values, offset, len);
}

/**
 * Lê um float e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o float lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(float* value) {
	return read_generic<float, float_to_byte>(value);
}

/**
 * Lê 'len' double's e armazena a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor que receberá os double's lidos.
 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 * @param len Quantidade de valores a serem lidos.
 * @return Número de valores efetivamente lidos.
 */
int BufferedInputStream::read(double* values, int offset, int len) {
	return read_generic<double, double_to_byte>(values, offset, len);
}

/**
 * Lê um double e armazena no ponteiro 'value'.
 * @param value Ponteiro que receberá o double lido.
 * @return True se foi possível ler o valor.
 */
bool BufferedInputStream::read(double* value) {
	return read_generic<double, double_to_byte>(value);
}

/**
 * Recarrega o buffer completamente a partir da entrada de fluxo de dados.
 * @return True se foi possível carregar o buffer.
 */
bool BufferedInputStream::load() {
	bool result = false;

	int pos_in = pos_out + quant;
	if(pos_in >= size){
		pos_in -= size;
	}

	if(in != NULL){
		int quant_read = in->read(&buffer, pos_in, (size - quant));
		if(quant_read > 0){
			result = true;
		}
		quant += quant_read;
	}

	return result;
}

/**
 * Limpa o buffer descartando dados.
 */
void BufferedInputStream::clear() {
	pos_out = 0;
	quant = 0;
}

/**
 * Limpa o buffer, fecha a entrada de fluxo de dados e libera os recursos.
 */
void BufferedInputStream::close() {
	buffer.clear();
	if(in != NULL){
		in->close();
		in = NULL;
	}
}

} /* namespace strm */
