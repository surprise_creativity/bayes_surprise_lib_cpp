/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BufferedInputStream.h   Entrada de fluxo de dados bufferiza- */
/*                                               da.                                 */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_STREAM_BUFFEREDINPUTSTREAM_H_
#define SRC_STREAM_BUFFEREDINPUTSTREAM_H_

#include "InputStream.h"

namespace strm {

/*
 * Fluxo de entrada de dados bufferizada.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 */

class BufferedInputStream {
public:
	BufferedInputStream(InputStream * _in);
	BufferedInputStream(InputStream * _in, int _size);
	virtual ~BufferedInputStream();

	int read(bool * values, int offset, int len);
 	bool read(bool * value);
 	int read(char * values, int offset, int len);
 	bool read(char * value);
 	int read(short * values, int offset, int len);
 	bool read(short * value);
 	int read(int * values, int offset, int len);
 	bool read(int * value);
 	int read(long * values, int offset, int len);
 	bool read(long * value);
 	int read(float * values, int offset, int len);
 	bool read(float * value);
 	int read(double * values, int offset, int len);
 	bool read(double * value);

 	bool load();
 	void clear();
 	void close();

private:

 	/**
 	 * Tamanho máximo do buffer.
 	 */
 	int size;

 	/**
 	 * Posição de saída de dados do buffer (onde será lido valores).
 	 */
 	int pos_out;

 	/**
 	 * Quantidade de bytes válidos no buffer (ainda não lidos).
 	 */
 	int quant;

 	/**
 	 * Buffer de dados (bytes). Vetor circular.
 	 */
 	std::vector<char> buffer;

 	/**
 	 * Fluxo de entrada de dados.
 	 */
 	InputStream * in;

 	/**
 	 * Lê 'len' valores do tipo 'type_values' e armazena a partir da posição 'offset' do vetor 'values'.
 	 * Obs.: realiza a conversão do tipo byte para 'type_values' através da union 'conv_type_to_byte'.
 	 * @param values Vetor que receberá os valores do tipo 'type_values' lidos.
 	 * @param offset Posição para armazenar o primeiro valor no vetor 'values'.
 	 * @param len Quantidade de valores a serem lidos.
 	 * @return Número de valores efetivamente lidos.
 	 */
 	template<typename type_values, typename conv_type_to_byte>
 	int read_generic(type_values * values, int offset, int len) {
 		int result = 0;
 		int len_index = (offset + len);
 		for(int i = offset;i < len_index;i++){
 			if(read_generic<type_values, conv_type_to_byte>((&values[i]))){
 				result++;
 			}else{
 				i = len_index;
 			}
 		}
 		return result;
 	}

 	/**
 	 * Lê um valor do tipo 'type_values' e armazena no ponteiro 'value'.
 	 * Obs.: realiza a conversão do tipo byte para 'type_values' através da union 'conv_type_to_byte'.
 	 * @param value Ponteiro que receberá o valor do tipo 'type_values' lido.
 	 * @return True se foi possível ler o valor.
 	 */
 	template<typename type_values, typename conv_type_to_byte>
 	bool read_generic(type_values * value) {
 		bool result = true;
 		conv_type_to_byte conv;
 		int tam_type = sizeof(type_values);

 		for(int i = 0;(i < tam_type) && result;i++){
 			if(!read((&conv.byte_value[i]))){
 				result = false;
 			}
 		}
 		if(result){
 			*value = conv.value;
 		}
 		return result;
 	}
};

} /* namespace strm */

#endif /* SRC_STREAM_BUFFEREDINPUTSTREAM_H_ */
