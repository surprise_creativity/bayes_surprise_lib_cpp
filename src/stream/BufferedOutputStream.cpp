/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BufferedOutputStream.cpp   Saída de fluxo de dados bufferi-  */
/*                                                  zada.                            */
/* --------------------------------------------------------------------------------- */

#include "BufferedOutputStream.h"
#include <cstdio>

namespace strm {

/**
 * Construtor com o fluxo de saída de dados passado via parâmetro (tamanho máximo do buffer padrão).
 * @param _out Fluxo de saída de dados.
 */
BufferedOutputStream::BufferedOutputStream(OutputStream * _out) {
	size = DEFAULT_SIZE_BUFF;
	pos_in = 0;
	quant = 0;
	out = _out;
}

/**
 * Construtor com o fluxo de saída de dados e o tamanho máximo do buffer passado via parâmetro.
 * @param _out Fluxo de saída de dados.
 * @param _size Tamanho máximo do buffer.
 */
BufferedOutputStream::BufferedOutputStream(OutputStream * _out, int _size) {
	if(_size <= 0){
		_size = DEFAULT_SIZE_BUFF;
	}
	size = _size;
	pos_in = 0;
	quant = 0;
	out = _out;
}

BufferedOutputStream::~BufferedOutputStream() {
	close();
}

/**
 * Escreve 'len' booleanos a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de booleanos a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(bool* values, int offset, int len) {
	int len_index = (offset + len);
	for(int i = offset;i < len_index;i++){
		write(values[i]);
	}
}

/**
 * Escreve um booleano na próxima posição do buffer.
 * @param value Booleano a ser escrito.
 */
void BufferedOutputStream::write(bool value) {
	char byte_value = 0;
	if(value){
		byte_value = 1;
	}
	write(byte_value);
}

/**
 * Escreve 'len' bytes(char) a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de bytes(char) a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(char* values, int offset, int len) {
	int len_index = (offset + len);
	for(int i = offset;i < len_index;i++){
		write(values[i]);
	}
}

/**
 * Escreve um byte(char) na próxima posição do buffer.
 * @param value Byte(char) a ser escrito.
 */
void BufferedOutputStream::write(char value) {
	if(quant == size){
		flush();
	}

	if(pos_in < (int)buffer.size()){
		buffer.at(pos_in) = value;
		pos_in++;
		quant++;
	}else{
		if((int)buffer.size() < size){
			buffer.push_back(value);
			pos_in++;
			quant++;
		}else{
			pos_in = 0;
			buffer.at(pos_in) = value;
			pos_in++;
			quant++;
		}
	}
}

/**
 * Escreve 'len' short's a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de short's a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(short* values, int offset, int len) {
	write_generic<short, short_to_byte>(values, offset, len);
}

/**
 * Escreve um short na próxima posição do buffer.
 * @param value Short a ser escrito.
 */
void BufferedOutputStream::write(short value) {
	write_generic<short, short_to_byte>(value);
}

/**
 * Escreve 'len' int's a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de int's a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(int* values, int offset, int len) {
	write_generic<int, int_to_byte>(values, offset, len);
}

/**
 * Escreve um int na próxima posição do buffer.
 * @param value Int a ser escrito.
 */
void BufferedOutputStream::write(int value) {
	write_generic<int, int_to_byte>(value);
}

/**
 * Escreve 'len' long's a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de long's a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(long* values, int offset, int len) {
	write_generic<long, long_to_byte>(values, offset, len);
}

/**
 * Escreve um long na próxima posição do buffer.
 * @param value Long a ser escrito.
 */
void BufferedOutputStream::write(long value) {
	write_generic<long, long_to_byte>(value);
}

/**
 * Escreve 'len' float's a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de float's a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(float* values, int offset, int len) {
	write_generic<float, float_to_byte>(values, offset, len);
}

/**
 * Escreve um float na próxima posição do buffer.
 * @param value Float a ser escrito.
 */
void BufferedOutputStream::write(float value) {
	write_generic<float, float_to_byte>(value);
}

/**
 * Escreve 'len' double's a partir da posição 'offset' do vetor 'values'.
 * @param values Vetor de double's a ser escrito.
 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 * @param len Quantidade de valores a serem escritos.
 */
void BufferedOutputStream::write(double* values, int offset, int len) {
	write_generic<double, double_to_byte>(values, offset, len);
}

/**
 * Escreve um double na próxima posição do buffer.
 * @param value Double a ser escrito.
 */
void BufferedOutputStream::write(double value) {
	write_generic<double, double_to_byte>(value);
}

/**
 * Força o esvaziamento do buffer e efetiva escrita no fluxo de saída de dados.
 */
void BufferedOutputStream::flush() {
	int pos_out = pos_in - quant;
	if(pos_out < 0){
		pos_out += size;
	}

	if(out != NULL){
		out->write(&buffer, pos_out, quant);
	}
	quant = 0;
}

/**
 * Esvaziar o buffer descartando os dados.
 */
void BufferedOutputStream::clear() {
	quant = 0;
}

/**
 * Finaliza o buffer, fecha o fluxo de saída de dados e libera os recursos.
 */
void BufferedOutputStream::close() {
	flush();
	buffer.clear();
	if(out != NULL){
		out->close();
		out = NULL;
	}
}

} /* namespace strm */
