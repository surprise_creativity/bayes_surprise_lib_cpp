/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BufferedOutputStream.h   Saída de fluxo de dados bufferiza-  */
/*                                                da.                                */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_STREAM_BUFFEREDOUTPUTSTREAM_H_
#define SRC_STREAM_BUFFEREDOUTPUTSTREAM_H_

#include "OutputStream.h"

namespace strm {

/**
 * Tamanho padrão de buffer.
 */
#define DEFAULT_SIZE_BUFF 65536

/**
 * Conversor de short (inteiro 2 bytes) para byte.
 */
typedef union _short_to_byte{
	short value;
	char byte_value[sizeof(short)];
}short_to_byte;

/**
 * Conversor de int (inteiro 4 bytes) para byte.
 */
typedef union _int_to_byte{
	int value;
	char byte_value[sizeof(int)];
}int_to_byte;

/**
 * Conversor de long (inteiro 8 bytes) para byte.
 */
typedef union _long_to_byte{
	long value;
	char byte_value[sizeof(long)];
}long_to_byte;

/**
 * Conversor de float (real 4 bytes) para byte.
 */
typedef union _float_to_byte{
	float value;
	char byte_value[sizeof(float)];
}float_to_byte;

/**
 * Conversor de double (real 8 bytes) para byte.
 */
typedef union _double_to_byte{
	double value;
	char byte_value[sizeof(double)];
}double_to_byte;

/*
 * Fluxo de saída de dados bufferizada.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 */

class BufferedOutputStream {
public:
	BufferedOutputStream(OutputStream * _out);
	BufferedOutputStream(OutputStream * _out, int _size);
	virtual ~BufferedOutputStream();

 	void write(bool * values, int offset, int len);
 	void write(bool value);
 	void write(char * values, int offset, int len);
 	void write(char value);
 	void write(short * values, int offset, int len);
 	void write(short value);
 	void write(int * values, int offset, int len);
 	void write(int value);
 	void write(long * values, int offset, int len);
 	void write(long value);
 	void write(float * values, int offset, int len);
 	void write(float value);
 	void write(double * values, int offset, int len);
 	void write(double value);

 	void flush();
 	void clear();
 	void close();

private:

 	/**
 	 * Tamanho máximo do buffer.
 	 */
 	int size;

 	/**
 	 * Posição de entrada de dados no buffer (local vago - possivelmente com dado já transferido).
 	 */
 	int pos_in;

 	/**
 	 * Quantidade de bytes válidos no buffer (ainda não transferidos).
 	 */
 	int quant;

 	/**
 	 * Buffer de dados (bytes). Vetor circular.
 	 */
 	std::vector<char> buffer;

 	/**
 	 * Fluxo de saída de dados.
 	 */
 	OutputStream * out;

 	/**
 	 * Escreve 'len' valores do tipo 'type_values' a partir da posição 'offset' do vetor 'values'.
 	 * Obs.: realiza a conversão do tipo 'type_values' para bytes através da union 'conv_type_to_byte'.
 	 * @param values Vetor de valores do tipo 'type_values' a ser escrito.
 	 * @param offset Posição do primeiro valor no vetor 'values' a ser escrito.
 	 * @param len Quantidade de valores a serem escritos.
 	 */
 	template<typename type_values, typename conv_type_to_byte>
 	void write_generic(type_values * values, int offset, int len){
 		int len_index = (offset + len);
 		for(int i = offset;i < len_index;i++){
 			write_generic<type_values, conv_type_to_byte>(values[i]);
 		}
 	}

 	/**
 	 * Escreve o valor do tipo 'type_values'.
 	 * Obs.: realiza a conversão do tipo 'type_values' para bytes através da union 'conv_type_to_byte'.
 	 * @param value Valor do tipo 'type_values' a ser escrito.
 	 */
 	template<typename type_values, typename conv_type_to_byte>
 	void write_generic(type_values value){
 		conv_type_to_byte conv;
 		int tam_type = sizeof(type_values);

 		conv.value = value;
 		for(int i = 0;i < tam_type;i++){
 			write(conv.byte_value[i]);
 		}
 	}
};

} /* namespace strm */

#endif /* SRC_STREAM_BUFFEREDOUTPUTSTREAM_H_ */
