/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: FileInputStream.cpp   Entrada de fluxo de dados por arquivo. */
/* --------------------------------------------------------------------------------- */

#include "FileInputStream.h"
#include "FileStreamExeption.h"

namespace strm {

/**
 * Construtor padrão (com o endereço do arquivo passado via parâmetro).
 * @param fileName Endereço e nome do arquivo a ser lido.
 */
FileInputStream::FileInputStream(const std::string& fileName) {
	fp = fopen(fileName.c_str(), "rb");
	if(fp == NULL){
		throw (new FileStreamExeption("Não foi possível abrir o arquivo."));
	}
}

FileInputStream::~FileInputStream() {
	close();
}

int FileInputStream::read(std::vector<char>* values, int offset, int quant) {
	int result = 0;
	if((quant > 0) && (offset >= 0) && (fp != NULL)){
		char * byte_values_array = new char[quant];

		result = read(byte_values_array, 0, quant);

		int len_index = offset + result;
		for(int i = offset;i < len_index;i++){
			values->at((i % values->size())) = byte_values_array[(i - offset)];
		}

		delete byte_values_array;
	}
	return result;
}

int FileInputStream::read(char* values, int offset, int len) {
	int result = 0;
	if((len > 0) && (offset >= 0) && (fp != NULL)){
		result = fread((&values[offset]), sizeof(char), len, fp);
	}
	return result;
}

int FileInputStream::read() {
	int result = -1;
	if(fp != NULL){
		char byte_value;
		if(fread((&byte_value), sizeof(char), 1, fp) == 1){
			result = byte_value;
		}
	}
	return result;
}

void FileInputStream::close() {
	if(fp != NULL){
		fclose(fp);
		fp = NULL;
	}
}

/**
 * Volta o ponteiro de leitura de arquivo para a posição inicial do arquivo.
 */
void FileInputStream::rewind_file() {
	if(fp != NULL){
		rewind(fp);
	}
}

} /* namespace strm */
