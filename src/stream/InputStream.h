/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: InputStream.h   Interface para um fluxo de entrada.          */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_STREAM_INPUTSTREAM_H_
#define SRC_STREAM_INPUTSTREAM_H_

#include <vector>

namespace strm {

/*
 * Interface para um fluxo de entrada de dados.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 */

class InputStream {
public:
	InputStream(){

	}

	virtual ~InputStream(){

	}

	/**
	 * Realiza a leitura de 'quant' bytes e armazena a partir do byte número 'offset' no vetor 'values'.
	 * @param values Vetor de bytes(char) que receberá os bytes lidos.
	 * @param offset Posição do primeiro byte a ser armazenado no vetor.
	 * @param quant Quantidade de bytes a serem lidos.
	 * @return Número de bytes efetivamente lidos.
	 */
	virtual int read(std::vector<char> * values, int offset, int quant) = 0;

	/**
	 * Realiza a leitura de 'len' bytes e armazena a partir do byte número 'offset' no vetor 'values'.
	 * @param values Vetor de bytes(char) que receberá os bytes lidos.
	 * @param offset Posição do primeiro byte a ser armazenado no vetor.
	 * @param len Quantidade de bytes a serem lidos.
	 * @return Número de bytes efetivamente lidos.
	 */
 	virtual int read(char * values, int offset, int len) = 0;

	/**
	 * Realiza a leitura de um byte na próxima posição do fluxo de entrada de dados.
	 * @return Valor do byte lido(como valor inteiro). Valor -1 indica erro.
	 */
 	virtual int read() = 0;

	/**
	 * Realiza o fechamento e liberação de recursos do fluxo de entrada de dados.
	 */
 	virtual void close() = 0;
};

} /* namespace strm */

#endif /* SRC_STREAM_INPUTSTREAM_H_ */
