/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ObjectArtifact.cpp   Abstração de um artefato genérico.      */
/* --------------------------------------------------------------------------------- */

#include "ObjectArtifact.h"

namespace art {

/**
 * Construtor do artefato.
 */
ObjectArtifact::ObjectArtifact() {

}

ObjectArtifact::~ObjectArtifact() {
	clear();
}

/**
 * Adiciona um valor boolean na próxima característica do artefato.
 * @param feature Valor booleano da característica.
 */
void ObjectArtifact::add(bool feature) {
	type_feature featureObj;
	featureObj.value.bool_feature = feature;
	featureObj.type_id = 1;
	features.push_back(featureObj);
}

/**
 * Adiciona um valor inteiro (maior que 0) na próxima característica do artefato.
 * @param feature Valor inteiro (maior que 0) da característica.
 */
void ObjectArtifact::add(int feature) {
	type_feature featureObj;
	featureObj.value.int_feature = feature;
	featureObj.type_id = 2;
	features.push_back(featureObj);
}

/**
 * Adiciona um valor real na próxima característica do artefato.
 * @param feature Valor real da característica.
 */
void ObjectArtifact::add(double feature) {
	type_feature featureObj;
	featureObj.value.double_feature = feature;
	featureObj.type_id = 3;
	features.push_back(featureObj);
}

/**
 * Indica se a característica da posição index do artefato é de tipo indefinido.
 * @param index Posição da característica no artefato.
 * @return True se a característica da posição index do artefato é de tipo indefinido.
 */
bool ObjectArtifact::isUndefined(int index) {
	return (getIdType(index) == 0);
}

/**
 * Indica se a característica da posição index do artefato é de tipo booleano.
 * @param index Posição da característica no artefato.
 * @return True se a característica da posição index do artefato é de tipo booleano.
 */
bool ObjectArtifact::isBoolean(int index) {
	return (getIdType(index) == 1);
}

/**
 * Indica se a característica da posição index do artefato é de tipo inteiro.
 * @param index Posição da característica no artefato.
 * @return True se a característica da posição index do artefato é de tipo inteiro.
 */
bool ObjectArtifact::isInteger(int index) {
	return (getIdType(index) == 2);
}

/**
 * Indica se a característica da posição index do artefato é de tipo real.
 * @param index Posição da característica no artefato.
 * @return True se a característica da posição index do artefato é de tipo real.
 */
bool ObjectArtifact::isDouble(int index) {
	return (getIdType(index) == 3);
}

/**
 * Lê, se possível (tipo compatível), o valor booleano da característica da posição index.
 * @param index Posição da característica no artefato.
 * @return Valor booleano da característica no artefato.
 */
bool ObjectArtifact::getValueBoolean(int index) {
	if(isBoolean(index)){
		return features[index].value.bool_feature;
	}else{
		return false;
	}
}

/**
 * Modifica, se possível (tipo compatível), o valor booleano da característica da posição index.
 * @param index Posição da característica no artefato.
 * @param feature Novo valor booleano da característica no artefato.
 */
void ObjectArtifact::setValueBoolean(int index, bool feature) {
	if(isBoolean(index)){
		features.at(index).value.bool_feature = feature;
	}
}

/**
 * Lê, se possível (tipo compatível), o valor inteiro da característica da posição index.
 * @param index Posição da característica no artefato.
 * @return Valor inteiro da característica no artefato.
 */
int ObjectArtifact::getValueInteger(int index) {
	if(isInteger(index)){
		return features[index].value.int_feature;
	}else{
		return 0;
	}
}

/**
 * Modifica, se possível (tipo compatível), o valor inteiro (maior que 0) da característica da posição index.
 * @param index Posição da característica no artefato.
 * @param feature Novo valor inteiro (maior que 0) da característica no artefato.
 */
void ObjectArtifact::setValueInteger(int index, int feature) {
	if(isInteger(index)){
		features.at(index).value.int_feature = feature;
	}
}

/**
 * Lê, se possível (tipo compatível), o valor real da característica da posição index.
 * @param index Posição da característica no artefato.
 * @return Valor real da característica no artefato.
 */
double ObjectArtifact::getValueDouble(int index) {
	if(isDouble(index)){
		return features[index].value.double_feature;
	}else{
		return 0.0;
	}
}

/**
 * Modifica, se possível (tipo compatível), o valor real da característica da posição index.
 * @param index Posição da característica no artefato.
 * @param feature Novo valor real da característica no artefato.
 */
void ObjectArtifact::setValueDouble(int index, double feature) {
	if(isDouble(index)){
		features.at(index).value.double_feature = feature;
	}
}

/**
 * Tamanho do artefato (em número de características).
 * @return Número de características do artefato.
 */
int ObjectArtifact::size() {
	return features.size();
}

/**
 * Deleta as características do artefato.
 */
void ObjectArtifact::clear() {
	features.clear();
}

/**
 * Gera uma cópia do artefato com toda as suas características.
 * @return Cópia do artefato.
 */
ObjectArtifact * ObjectArtifact::getCopy() {
	ObjectArtifact * newObjArt = new ObjectArtifact();
	for(int index = 0;index < size();index++){
		char typeFeature = getIdType(index);
		if(typeFeature == 1){
			bool feature = getValueBoolean(index);
			newObjArt->add(feature);
		}else if(typeFeature == 2){
			int feature = getValueInteger(index);
			newObjArt->add(feature);
		}else if(typeFeature == 3){
			double feature = getValueDouble(index);
			newObjArt->add(feature);
		}
	}
	return newObjArt;
}

/**
 * Copia os valores do artefato atual para o artefato destiny.
 * @param destiny Artefato que irá receber uma cópia das características.
 */
void ObjectArtifact::copyTo(ObjectArtifact * destiny) {
	destiny->clear();
	for(int index = 0;index < size();index++){
		char typeFeature = getIdType(index);
		if(typeFeature == 1){
			bool feature = getValueBoolean(index);
			destiny->add(feature);
		}else if(typeFeature == 2){
			int feature = getValueInteger(index);
			destiny->add(feature);
		}else if(typeFeature == 3){
			double feature = getValueDouble(index);
			destiny->add(feature);
		}
	}
}

// Indica o Id do tipo da característica na posição index do artefato.
// @param index Posição da característica no artefato.
// @return O valor do Id do tipo da característica na posição index do artefato.
char ObjectArtifact::getIdType(int index) {
	if(index >= 0 && index < (int)features.size()){
		return features[index].type_id;
	}else{
		return 0;
	}
}

} /* namespace art */
