/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ObjectArtifact.h   Abstração de um artefato genérico.        */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_ARTIFACT_OBJECTARTIFACT_H_
#define SRC_SURP_ARTIFACT_OBJECTARTIFACT_H_

#include <vector>

namespace art {

/**
 * Classe que abstrai um artefato.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class ObjectArtifact {
public:
	ObjectArtifact();
	virtual ~ObjectArtifact();

	void add(bool feature);
	void add(int feature);
	void add(double feature);

	bool isUndefined(int index);
	bool isBoolean(int index);
	bool isInteger(int index);
	bool isDouble(int index);

	bool getValueBoolean(int index);
	void setValueBoolean(int index, bool feature);

	int getValueInteger(int index);
	void setValueInteger(int index, int feature);

	double getValueDouble(int index);
	void setValueDouble(int index, double feature);

	int size();
	void clear();
	ObjectArtifact * getCopy();
	void copyTo(ObjectArtifact * destiny);

private:
	typedef union _value_feature{
		bool bool_feature;
		int int_feature;
		double double_feature;
	}value_feature;

	typedef struct _type_feature{
		char type_id;
		value_feature value;
	}type_feature;

	std::vector<type_feature> features;

	char getIdType(int index);
};

} /* namespace art */

#endif /* SRC_SURP_ARTIFACT_OBJECTARTIFACT_H_ */
