/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ArtifactEval.cpp   Avaliador de surpresa de artefatos.       */
/* --------------------------------------------------------------------------------- */

#include "ArtifactEval.h"

/**
 * Avaliador de Modelos de Surpresa Bayesiana.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

namespace evl {

/**
 * Indica se o Modelo Bayesiano objMdl é compatível (nos tipos das características) com o artefato objArt.
 * @param objMdl Objeto do Modelo Bayesiano.
 * @param objArt Objeto do artefato.
 * @return True se o objeto do Modelo Bayesiano é compatível com o objeto do artefato.
 */
bool isCompatible(mdl::ObjectModel * objMdl, art::ObjectArtifact * objArt){
	bool result = true;

	if(objMdl->size() == objArt->size()){
		for(int i = 0;(i < objMdl->size()) && (result);i++){
			if((objMdl->isBoolean(i) && (!objArt->isBoolean(i))) || (objMdl->isInteger(i) && (!objArt->isInteger(i))) || (objMdl->isDouble(i) && (!objArt->isDouble(i)))){
				result = false;
			}
		}
	}else{
		result = false;
	}

	return result;
}

/**
 * Calcula a surpresa total do artefato objArt em relação ao Modelo Bayesiano objMdl.
 * @param objMdl Objeto do Modelo Bayesiano.
 * @param objArt Objeto do artefato.
 * @param itg Objeto integrador (realiza a integração entre as surpresas das características).
 * @return Valor de surpresa do artefato completo.
 */
double getSurprise(mdl::ObjectModel * objMdl, art::ObjectArtifact * objArt, Integrater * itg){
	double result = 0.0;

	if(objMdl->size() == objArt->size()){
		for(int i = 0;i < objMdl->size();i++){
			if(objMdl->isBoolean(i) && objArt->isBoolean(i)){
				result = itg->integrateSurprise(result, itg->normSurpriseBoolean(objMdl->getSurpriseBoolean(i, objArt->getValueBoolean(i))));
			}else if(objMdl->isInteger(i) && objArt->isInteger(i)){
				result = itg->integrateSurprise(result, itg->normSurpriseInteger(objMdl->getSurpriseInteger(i, objArt->getValueInteger(i))));
			}else if(objMdl->isDouble(i) && objArt->isDouble(i)){
				result = itg->integrateSurprise(result, itg->normSurpriseDouble(objMdl->getSurpriseDouble(i, objArt->getValueDouble(i))));
			}else{
				result = -1.0;
				i = objMdl->size();
			}
		}
	}else{
		result = -1.0;
	}

	return result;
}

/**
 * Apreende o artefato objArt no Modelo Bayesiano objMdl.
 * @param objMdl Objeto do Modelo Bayesiano.
 * @param objArt Objeto do artefato.
 */
void learn(mdl::ObjectModel * objMdl, art::ObjectArtifact * objArt){
	if(objMdl->size() == objArt->size()){
		for(int i = 0;i < objMdl->size();i++){
			if(objMdl->isBoolean(i) && objArt->isBoolean(i)){
				objMdl->learnBoolean(i, objArt->getValueBoolean(i));
			}else if(objMdl->isInteger(i) && objArt->isInteger(i)){
				objMdl->learnInteger(i, objArt->getValueInteger(i));
			}else if(objMdl->isDouble(i) && objArt->isDouble(i)){
				objMdl->learnDouble(i, objArt->getValueDouble(i));
			}
		}
	}
}

} /* namespace evl */
