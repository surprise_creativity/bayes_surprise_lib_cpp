/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: Integrater.h   Interface para compor o valor de surpresa.    */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_EVALUATION_INTEGRATER_H_
#define SRC_SURP_EVALUATION_INTEGRATER_H_

namespace evl {

/**
 * Integrador (ou agregador) para ajustar os valores de surpresa entre tipos de dados diferentes.
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class Integrater {
public:
	Integrater(){

	}

	virtual ~Integrater(){

	}

	/**
	 * Normaliza o valor de surpresa para o tipo booleano.
	 * @param surprise Valor de surpresa não normalizado.
	 * @return Valor de surpresa normalizada.
	 */
	virtual double normSurpriseBoolean(double surprise) = 0;

	/**
	 * Normaliza o valor de surpresa para o tipo inteiro (maior que 0).
	 * @param surprise Valor de surpresa não normalizado.
	 * @return Valor de surpresa normalizada.
	 */
	virtual double normSurpriseInteger(double surprise) = 0;

	/**
	 * Normaliza o valor de surpresa para o tipo real.
	 * @param surprise Valor de surpresa não normalizado.
	 * @return Valor de surpresa normalizada.
	 */
	virtual double normSurpriseDouble(double surprise) = 0;

	/**
	 * Operação de integração entre dois valores de surpresa.
	 * @param surprise1 Surpresa de uma característica.
	 * @param surprise2 Surpresa de uma segunda característica.
	 * @return Surpresa final após a integração.
	 */
	virtual double integrateSurprise(double surprise1, double surprise2) = 0;
};

} /* namespace evl */

#endif /* SRC_SURP_EVALUATION_INTEGRATER_H_ */
