/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: IntegerModel.h   M. Bayesiano genérico para tipo inteiro.    */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_MODEL_INTEGERMODEL_H_
#define SRC_SURP_MODEL_INTEGERMODEL_H_

#include "TypeModel.h"

namespace mdl {

/**
 * Modelo Bayesiano para calculo de surpresa em tipo de dados inteiro (maior que 0).
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class IntegerModel: public TypeModel {
public:
	IntegerModel(){

	}

	virtual ~IntegerModel(){

	}

	/**
	 * Apreende um conjunto de dados com amount_total números inteiros (maiores que 0), onde mean é a média amostral destes valores.
	 * @param mean Média amostral do conjunto de números inteiros (maiores que 0).
	 * @param amount_total Quantidade total de números inteiros (maiores que 0).
	 */
	virtual void learn(double mean, int amount_total) = 0;

	/**
	 * Apreende um conjunto de números inteiros (maiores que 0) passados via lista.
	 * @param values Lista de números inteiros (maiores que 0).
	 */
	virtual void learn(std::vector<int> values) = 0;

	/**
	 * Apreende um número inteiro (maior que 0).
	 * @param value Número inteiro (maior que 0).
	 */
	virtual void learn(int value) = 0;

	/**
	 * Calcula a surpresa de um conjunto de dados com amount_total números inteiros (maiores que 0), onde mean é a média amostral destes valores.
	 * @param mean Média amostral do conjunto de números inteiros (maiores que 0).
	 * @param amount_total Quantidade total de números inteiros (maiores que 0).
	 * @return Valor de surpresa.
	 */
	virtual double getSurprise(double mean, int amount_total) = 0;

	/**
	 * Calcula a surpresa de um conjunto de números inteiros (maiores que 0) passados via lista.
	 * @param values Lista de números inteiros (maiores que 0).
	 * @return Valor de surpresa.
	 */
	virtual double getSurprise(std::vector<int> values) = 0;

	/**
	 * Calcula a surpresa de um número inteiro (maior que 0).
	 * @param value Número inteiro (maior que 0).
	 * @return Valor de surpresa.
	 */
	virtual double getSurprise(int value) = 0;
};

} /* namespace mdl */

#endif /* SRC_SURP_MODEL_INTEGERMODEL_H_ */
