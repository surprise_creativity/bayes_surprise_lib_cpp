/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ObjectModel.cpp   Abstração de um conjunto de Modelos.       */
/* --------------------------------------------------------------------------------- */

#include "ObjectModel.h"
#include "../../stream/FileOutputStream.h"
#include "../../stream/FileInputStream.h"
#include "../../stream/FileStreamExeption.h"

namespace mdl {

/**
 * Construtor.
 */
ObjectModel::ObjectModel() {

}

ObjectModel::~ObjectModel() {
	clear();
}

/**
 * Adiciona um Modelo Bayesiano para característica booleana.
 * @param featureModel Modelo Bayesiano para dado booleano.
 */
void ObjectModel::add(BooleanModel* featureModel) {
	featuresModel.push_back(featureModel);
}

/**
 * Adiciona um Modelo Bayesiano para característica inteira (maior que 0).
 * @param featureModel Modelo Bayesiano para dado inteiro (maior que 0).
 */
void ObjectModel::add(IntegerModel* featureModel) {
	featuresModel.push_back(featureModel);
}

/**
 * Adiciona um Modelo Bayesiano para característica real.
 * @param featureModel Modelo Bayesiano para dado real.
 */
void ObjectModel::add(DoubleModel* featureModel) {
	featuresModel.push_back(featureModel);
}

/**
 * Indica se a característica da posição index dos modelos é de tipo indefinido.
 * @param index Posição da característica nos modelos.
 * @return True se a característica da posição index dos modelos é de tipo indefinido.
 */
bool ObjectModel::isUndefined(int index) {
	return (getIdType(index) == 0);
}

/**
 * Indica se a característica da posição index dos modelos é de tipo booleano.
 * @param index Posição da característica nos modelos.
 * @return True se a característica da posição index dos modelos é de tipo booleano.
 */
bool ObjectModel::isBoolean(int index) {
	return (getIdType(index) == 1);
}

/**
 * Indica se a característica da posição index dos modelos é de tipo inteiro (maior que 0).
 * @param index Posição da característica nos modelos.
 * @return True se a característica da posição index dos modelos é de tipo inteiro (maior que 0).
 */
bool ObjectModel::isInteger(int index) {
	return (getIdType(index) == 2);
}

/**
 * Indica se a característica da posição index dos modelos é de tipo real.
 * @param index Posição da característica nos modelos.
 * @return True se a característica da posição index dos modelos é de tipo real.
 */
bool ObjectModel::isDouble(int index) {
	return (getIdType(index) == 3);
}

/**
 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor booleano da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor booleano sobre o qual será medida a surpresa da característica.
 * @return Valor de surpresa.
 */
double ObjectModel::getSurpriseBoolean(int index, bool value) {
	if(isBoolean(index)){
		BooleanModel * featureModel = (BooleanModel*)featuresModel.at(index);
		return (featureModel->getSurprise(value));
	}else{
		return 0.0;
	}
}

/**
 * Apreende, se possível (tipo compatível), o valor booleano da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor booleano que será apreendido.
 */
void ObjectModel::learnBoolean(int index, bool value) {
	if(isBoolean(index)){
		BooleanModel * featureModel = (BooleanModel*)featuresModel.at(index);
		featureModel->learn(value);
	}
}

/**
 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor inteiro (maior que 0) da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor inteiro (maior que 0) sobre o qual será medida a surpresa da característica.
 * @return Valor de surpresa.
 */
double ObjectModel::getSurpriseInteger(int index, int value) {
	if(isInteger(index)){
		IntegerModel * featureModel = (IntegerModel*)featuresModel.at(index);
		return (featureModel->getSurprise(value));
	}else{
		return 0.0;
	}
}

/**
 * Apreende, se possível (tipo compatível), o valor inteiro (maior que 0) da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor inteiro (maior que 0) que será apreendido.
 */
void ObjectModel::learnInteger(int index, int value) {
	if(isInteger(index)){
		IntegerModel * featureModel = (IntegerModel*)featuresModel.at(index);
		featureModel->learn(value);
	}
}

/**
 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor real da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor real sobre o qual será medida a surpresa da característica.
 * @return Valor de surpresa.
 */
double ObjectModel::getSurpriseDouble(int index, double value) {
	if(isDouble(index)){
		DoubleModel * featureModel = (DoubleModel*)featuresModel.at(index);
		return (featureModel->getSurprise(value));
	}else{
		return 0.0;
	}
}

/**
 * Apreende, se possível (tipo compatível), o valor real da característica da posição index.
 * @param index Posição da característica nos modelos.
 * @param value Valor real que será apreendido.
 */
void ObjectModel::learnDouble(int index, double value) {
	if(isDouble(index)){
		DoubleModel * featureModel = (DoubleModel*)featuresModel.at(index);
		featureModel->learn(value);
	}
}

/**
 * Escreve os parâmetros de todos os modelos bayesianos do conjunto em uma saída buferrizada (ex: arquivo).
 * @param bos Saída bufferizada.
 * @return True se foi possível escrever, false em caso contrário.
 */
bool ObjectModel::write(strm::BufferedOutputStream* bos) {
	bool result = true;
	for(int index = 0;(index < (int)featuresModel.size()) && result;index++){
		TypeModel * featureModel = featuresModel.at(index);
		result = featureModel->write(bos);
	}
	return result;
}

/**
 * Escreve os parâmetros de todos os modelos bayesianos do conjunto em um arquivo binário.
 * @param fileName Endereço do arquivo binário a ser escrito (se existir, sobreescreve).
 * @return True se foi possível escrever, false em caso contrário.
 */
bool ObjectModel::write(const std::string& fileName) {
	bool result = true;
	try{
		strm::FileOutputStream * fos = new strm::FileOutputStream(fileName);
		strm::BufferedOutputStream * bos = new strm::BufferedOutputStream(fos);
		result = write(bos);
		bos->close();
		fos->close();
		delete bos;
		delete fos;
	}catch(strm::FileStreamExeption * fex){
		result = false;
		fex->printMsg();
		delete fex;
    }
	return result;
}

/**
 * Lê os parâmetros de todos os modelos bayesianos do conjunto de uma entrada buferrizada (ex: arquivo).
 * @param bis Entrada bufferizada.
 * @return True se foi possível ler, false em caso contrário.
 */
bool ObjectModel::read(strm::BufferedInputStream* bis) {
	bool result = true;
	for(int index = 0;(index < (int)featuresModel.size()) && result;index++){
		TypeModel * featureModel = featuresModel.at(index);
		result = featureModel->read(bis);
	}
	return result;
}

/**
 * Lê os parâmetros de todos os modelos bayesianos do conjunto de um arquivo binário.
 * @param fileName Endereço do arquivo binário a ser lido.
 * @return True se foi possível ler, false em caso contrário.
 */
bool ObjectModel::read(const std::string& fileName) {
	bool result = true;
	try{
		strm::FileInputStream * fis = new strm::FileInputStream(fileName);
		strm::BufferedInputStream * bis = new strm::BufferedInputStream(fis);
		result = read(bis);
		bis->close();
		fis->close();
		delete bis;
		delete fis;
	}catch(strm::FileStreamExeption * fex){
		result = false;
		fex->printMsg();
		delete fex;
    }
	return result;
}

/**
 * Tamanho do conjunto de Modelos Bayesianos (em número de modelos/características).
 * @return Número de Modelos Bayesianos no conjunto.
 */
int ObjectModel::size() {
	return featuresModel.size();
}

/**
 * Deleta os Modelos Bayesianos do conjunto.
 */
void ObjectModel::clear() {
	featuresModel.clear();
}

/**
 * Gera uma cópia do conjunto de modelos com a referência a toda os seus Modelos Bayesianos.
 * @return Cópia do conjunto de modelos.
 */
ObjectModel* ObjectModel::getCopy() {
	ObjectModel * newObjMdl = new ObjectModel();
	for(int index = 0;index < size();index++){
		char typeFeature = getIdType(index);
		if(typeFeature == 1){
			BooleanModel * featureModel = (BooleanModel*)featuresModel.at(index);
			newObjMdl->add(featureModel);
		}else if(typeFeature == 2){
			IntegerModel * featureModel = (IntegerModel*)featuresModel.at(index);
			newObjMdl->add(featureModel);
		}else if(typeFeature == 3){
			DoubleModel * featureModel = (DoubleModel*)featuresModel.at(index);
			newObjMdl->add(featureModel);
		}
	}
	return newObjMdl;
}

/**
 * Copia as referências do conjunto de modelos atual para o conjunto de modelos destiny.
 * @param destiny Conjunto de modelos que irá receber uma cópia das referências dos Modelos Bayesianos.
 */
void ObjectModel::copyTo(ObjectModel* destiny) {
	destiny->clear();
	for(int index = 0;index < size();index++){
		char typeFeature = getIdType(index);
		if(typeFeature == 1){
			BooleanModel * featureModel = (BooleanModel*)featuresModel.at(index);
			destiny->add(featureModel);
		}else if(typeFeature == 2){
			IntegerModel * featureModel = (IntegerModel*)featuresModel.at(index);
			destiny->add(featureModel);
		}else if(typeFeature == 3){
			DoubleModel * featureModel = (DoubleModel*)featuresModel.at(index);
			destiny->add(featureModel);
		}
	}
}

// Indica o Id do tipo da característica na posição index dos modelos.
// @param index Posição da característica nos modelos.
// @return O valor do Id do tipo da característica na posição index dos modelos.
char ObjectModel::getIdType(int index) {
	if(index >= 0 && index < (int)featuresModel.size()){
		return featuresModel[index]->getIdType();
	}else{
		return 0;
	}
}

} /* namespace mdl */
