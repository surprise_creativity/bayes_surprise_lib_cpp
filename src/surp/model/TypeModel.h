/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: TypeModel.h   Interface polimórfica dos tipos de modelos.    */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_MODEL_TYPEMODEL_H_
#define SRC_SURP_MODEL_TYPEMODEL_H_

#include "../../stream/BufferedOutputStream.h"
#include "../../stream/BufferedInputStream.h"

namespace mdl {

/**
 * Interface que generaliza os Modelos Bayesianos (qualquer tipo de dados).
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class TypeModel {
public:
	TypeModel(){

	}

	virtual ~TypeModel(){

	}

	/**
	 * Indica o Id do tipo de dados do Modelo Bayesiano.
	 * @return Retorna 0 se for indefinido; 1, se for Booleano; 2, se for Inteiro (maior que 0);
	 * 3, se for real.
	 */
	virtual char getIdType() = 0;

	/**
	 * Função para escrita dos parâmetros da distribuição em uma saída bufferizada (ex: arquivo).
	 * @param bos Saída bufferizada.
	 * @return True se foi possível escrever, false em caso contrário.
	 */
	virtual bool write(strm::BufferedOutputStream * bos) = 0;

	/**
	 * Função para leitura dos parâmetros da distribuição de um entrada bufferizada (ex: arquivo).
	 * @param bis Entrada bufferizada.
	 * @return True se foi possível ler, false em caso contrário.
	 */
	virtual bool read(strm::BufferedInputStream * bis) = 0;
};

} /* namespace mdl */

#endif /* SRC_SURP_MODEL_TYPEMODEL_H_ */
