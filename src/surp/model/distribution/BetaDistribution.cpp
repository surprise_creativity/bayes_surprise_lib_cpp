/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BetaDistribution.cpp   Modelo para dado booleano.            */
/* --------------------------------------------------------------------------------- */

#include "BetaDistribution.h"
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>
#include <cmath>

namespace dist {

/**
 * Construtor padrão.
 */
BetaDistribution::BetaDistribution() {
	a1 = 1;
	b1 = 1;
}

/**
 * Construtor para um valor de média a priori (entre 0 e 1).
 * @param mean_priori Média priori alvo (conhecimento prévio sobre o mecanismo).
 */
BetaDistribution::BetaDistribution(double mean_priori) {
	if(mean_priori < 0){
		mean_priori = 0;
	}else if(mean_priori > 1){
		mean_priori = 1;
	}
	if(mean_priori > 0 && mean_priori < 1){
		b1 = 1;
		a1 = mean_priori / (1.0 - mean_priori);
	}else if(mean_priori == 1){
		b1 = 1;
		a1 = 100;
	}else if(mean_priori == 0){
		b1 = 100;
		a1 = 1;
	}
}

/**
 * Construtor com parâmetros.
 * @param _a1 Parametro associado com estado true.
 * @param _b1 Parametro associado com estado false.
 */
BetaDistribution::BetaDistribution(double _a1, double _b1) {
	if(_a1 < 1){
		_a1 = 1;
	}
	if(_b1 < 1){
		_b1 = 1;
	}
	a1 = _a1;
	b1 = _b1;
}

BetaDistribution::~BetaDistribution() {

}

void BetaDistribution::learn(int amount_true, int amount_total) {
	a1 = a1 + amount_true;
	b1 = b1 + (amount_total - amount_true);
}

void BetaDistribution::learn(std::vector<bool> values) {
	int amount_true = 0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		if(values[i]){
			amount_true++;
		}
	}
	learn(amount_true, amount_total);
}

void BetaDistribution::learn(bool value) {
	int amount_true = 0;
	int amount_total = 1;
	if(value){
		amount_true++;
	}
	learn(amount_true, amount_total);
}

double BetaDistribution::getSurprise(int amount_true, int amount_total) {
	double result = 0.0;

	if((amount_total >= 100) && (amount_true < amount_total) && (amount_true > 0)){ // p deve pertencer ao intervalo (0,1)
		result = surpriseApproximation(amount_true, amount_total);
	}else{
		result = surprise(amount_true, amount_total);
	}

	return result;
}

double BetaDistribution::getSurprise(std::vector<bool> values) {
	int amount_true = 0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		if(values[i]){
			amount_true++;
		}
	}
	return getSurprise(amount_true, amount_total);
}

double BetaDistribution::getSurprise(bool value) {
	int amount_true = 0;
	int amount_total = 1;
	if(value){
		amount_true++;
	}
	return getSurprise(amount_true, amount_total);
}

char BetaDistribution::getIdType() {
	return 1;
}

bool BetaDistribution::write(strm::BufferedOutputStream * bos) {
	bool result = true;

	bos->write(a1);
	bos->write(b1);
	bos->flush();

	return result;
}

bool BetaDistribution::read(strm::BufferedInputStream * bis) {
	bool result = true;

	if(!bis->read(&a1)){
		result = false;
	}
	if(result && (!bis->read(&b1))){
		result = false;
	}

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula exata.
// @param amount_true Quantidade de booleanos true.
// @param amount_total Quantidade total de booleanos (true e false).
// @return Valor de surpresa.
double BetaDistribution::surprise(int amount_true, int amount_total) {
	double result = 0.0;
	double const_beta = gsl_sf_beta((a1 + amount_true), (b1 + (amount_total - amount_true)))
			          / gsl_sf_beta(a1, b1);
	double digamma_sum = gsl_sf_psi(a1 + b1);

	result = log(const_beta) + amount_true * (digamma_sum - gsl_sf_psi(a1))
			                       + (amount_total - amount_true) * (digamma_sum - gsl_sf_psi(b1));
	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula aproximada.
// @param amount_true Quantidade de booleanos true.
// @param amount_total Quantidade total de booleanos (true e false).
// @return Valor de surpresa.
double BetaDistribution::surpriseApproximation(int amount_true,
		int amount_total) {
	double result = 0.0;

	double sum_inv_k = 0.0;
	for(int k = (int)a1;k <= ((2 * ((int)a1) - 1));k++){
		sum_inv_k += (1.0 / ((double)k));
	}

	double p = ((double)amount_true) / ((double)amount_total);
	double Hp = (- p * log(p) - (1 - p) * log(1 - p));

	result = (((double)amount_total) * (sum_inv_k - Hp));

	return result;
}

} /* namespace dist */
