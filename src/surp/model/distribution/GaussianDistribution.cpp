/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: GaussianDistribution.cpp   Modelo para dado real(var fixa).  */
/* --------------------------------------------------------------------------------- */

#include "GaussianDistribution.h"
#include <cmath>

namespace dist {

/**
 * Construtor padrão (variância fixa em 1.0).
 */
GaussianDistribution::GaussianDistribution() {
	mean1 = 0.0;
	variance1 = 1.0;
	variance_data = 1.0;
}

/**
 * Construtor para um valor de variância fixa passado.
 * @param _variance_data Variância fixa das amostras de dados.
 */
GaussianDistribution::GaussianDistribution(double _variance_data) {
	if(_variance_data < 0.1){
		_variance_data = 0.1;
	}
	mean1 = 0.0;
	variance1 = 1.0;
	variance_data = _variance_data;
}

/**
 * Construtor para valor de variância fixa passado e parâmetros de uma gaussiana para a média.
 * @param _mean1 Média da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
 * @param _variance1 Variância da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
 * @param _variance_data Variância (fixa) das amostras de dados.
 */
GaussianDistribution::GaussianDistribution(double _mean1, double _variance1,
		double _variance_data) {
	if(_variance1 < 0.1){
		_variance1 = 0.1;
	}
	if(_variance_data < 0.1){
		_variance_data = 0.1;
	}
	mean1 = _mean1;
	variance1 = _variance1;
	variance_data = _variance_data;
}

GaussianDistribution::~GaussianDistribution() {

}

void GaussianDistribution::learn(double mean, double variance,
		int amount_total) {
	double inv_variance = ((1.0 / variance1) + (((double)amount_total) / variance_data));
	mean1 = ((mean1 / variance1) + ((amount_total * mean) / variance_data)) / inv_variance;
	variance1 = (1.0 / inv_variance);
}

void GaussianDistribution::learn(std::vector<double> values) {
	double mean = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		double value = values[i];
		mean += value;
	}
	if(amount_total > 0){
		mean = mean / ((double)amount_total);
	}
	learn(mean, variance_data, amount_total);
}

void GaussianDistribution::learn(double value) {
	double mean = value;
	int amount_total = 1;
	learn(mean, variance_data, amount_total);
}

double GaussianDistribution::getSurprise(double mean, double variance,
		int amount_total) {
	double result = 0.0;
	if(amount_total >= 100){
		result = surpriseApproximation(mean, variance, amount_total);
	}else{
		result = surprise(mean, variance, amount_total);
	}
	return result;
}

double GaussianDistribution::getSurprise(std::vector<double> values) {
	double mean = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		double value = values[i];
		mean += value;
	}
	if(amount_total > 0){
		mean = mean / ((double)amount_total);
	}
	return getSurprise(mean, variance_data, amount_total);
}

double GaussianDistribution::getSurprise(double value) {
	double mean = value;
	int amount_total = 1;
	return getSurprise(mean, variance_data, amount_total);
}

char GaussianDistribution::getIdType() {
	return 3;
}

bool GaussianDistribution::write(strm::BufferedOutputStream* bos) {
	bool result = true;

	bos->write(mean1);
	bos->write(variance1);
	bos->write(variance_data);
	bos->flush();

	return result;
}

bool GaussianDistribution::read(strm::BufferedInputStream* bis) {
	bool result = true;

	if(!bis->read(&mean1)){
		result = false;
	}
	if(result && (!bis->read(&variance1))){
		result = false;
	}
	if(result && (!bis->read(&variance_data))){
		result = false;
	}

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula exata.
// @param mean Média amostral do conjunto de valores reais.
// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
// @param amount_total Quantidade total de valores reais.
// @return Valor de surpresa.
double GaussianDistribution::surprise(double mean, double variance,
		int amount_total) {
	double result = 0.0;

	double sum_variance = (variance_data + amount_total * variance1);

	double const_variance = sqrt(variance_data / sum_variance);

	double deaviation = (mean1 - mean);
	deaviation = deaviation * deaviation;

	double variance_relative = (amount_total * variance1) / (2.0 * variance_data);

	result = log(const_variance) + variance_relative
			+ ((amount_total * variance_relative) * (deaviation / sum_variance));

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula aproximada.
// @param mean Média amostral do conjunto de valores reais.
// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
// @param amount_total Quantidade total de valores reais.
// @return Valor de surpresa.
double GaussianDistribution::surpriseApproximation(double mean, double variance,
		int amount_total) {
	double result = 0.0;

	double deaviation = (mean1 - mean);
	deaviation = deaviation * deaviation;

	result = ((((double)amount_total) / (2.0 * variance_data)) * (variance1 + deaviation));

	return result;
}

} /* namespace dist */
