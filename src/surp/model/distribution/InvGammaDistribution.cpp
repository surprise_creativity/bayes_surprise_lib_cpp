/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*               File: InvGammaDistribution.cpp   Modelo para dado real(mean fixa).  */
/* --------------------------------------------------------------------------------- */

#include "InvGammaDistribution.h"
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>
#include <cmath>

namespace dist {

/**
 * Construtor padrão.
 */
InvGammaDistribution::InvGammaDistribution() {
	v1 = 1;
	s1 = 1;
	mean_data = 0.0;
}

/**
 * Construtor para um valor de média fixa passado.
 * @param _mean_data Média (fixa) das amostras de dados.
 */
InvGammaDistribution::InvGammaDistribution(double _mean_data) {
	v1 = 1;
	s1 = 1;
	mean_data = _mean_data;
}

/**
 * Construtor para valores de parâmetros a priori passados.
 * @param _v1 Primeiro parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
 * @param _s1 Segundo parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
 * @param _mean_data Média (fixa) das amostras de dados.
 */
InvGammaDistribution::InvGammaDistribution(double _v1, double _s1,
		double _mean_data) {
	if(_v1 < 0.1){
		_v1 = 0.1;
	}
	if(_s1 < 0.1){
		_s1 = 0.1;
	}
	v1 = _v1;
	s1 = _s1;
	mean_data = _mean_data;
}

InvGammaDistribution::~InvGammaDistribution() {

}

void InvGammaDistribution::learn(double mean, double variance,
		int amount_total) {
	double v2 = (v1 + amount_total);
	s1 = ((v1 * s1 + amount_total * variance) / v2);
	v1 = v2;
}

void InvGammaDistribution::learn(std::vector<double> values) {
	double variance = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		double value = values[i];
		value = (value - mean_data);
		variance += (value * value);
	}
	if(amount_total > 0){
		variance = variance / ((double)amount_total);
	}
	learn(mean_data, variance, amount_total);
}

void InvGammaDistribution::learn(double value) {
	double variance = (value - mean_data);
	variance = variance * variance;
	int amount_total = 1;
	learn(mean_data, variance, amount_total);
}

double InvGammaDistribution::getSurprise(double mean, double variance,
		int amount_total) {
	double result = 0.0;
	if((amount_total >= 100) && (variance > 0)){
		result = surpriseApproximation(mean, variance, amount_total);
	}else{
		result = surprise(mean, variance, amount_total);
	}
	return result;
}

double InvGammaDistribution::getSurprise(std::vector<double> values) {
	double variance = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		double value = values[i];
		value = (value - mean_data);
		variance += (value * value);
	}
	if(amount_total > 0){
		variance = variance / ((double)amount_total);
	}
	return getSurprise(mean_data, variance, amount_total);
}

double InvGammaDistribution::getSurprise(double value) {
	double variance = (value - mean_data);
	variance = variance * variance;
	int amount_total = 1;
	return getSurprise(mean_data, variance, amount_total);
}

char InvGammaDistribution::getIdType() {
	return 3;
}

bool InvGammaDistribution::write(strm::BufferedOutputStream* bos) {
	bool result = true;

	bos->write(v1);
	bos->write(s1);
	bos->write(mean_data);
	bos->flush();

	return result;
}

bool InvGammaDistribution::read(strm::BufferedInputStream* bis) {
	bool result = true;

	if(!bis->read(&v1)){
		result = false;
	}
	if(result && (!bis->read(&s1))){
		result = false;
	}
	if(result && (!bis->read(&mean_data))){
		result = false;
	}

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula exata.
// @param mean Média amostral do conjunto de valores reais.
// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
// @param amount_total Quantidade total de valores reais.
// @return Valor de surpresa.
double InvGammaDistribution::surprise(double mean, double variance,
		int amount_total) {
	double result = 0.0;

	double v2 = (v1 + amount_total);
	double s2 = ((v1 * s1 + amount_total * variance) / v2);

	double v1_half = (v1 / 2.0);
	double v2_half = (v2 / 2.0);

	double const_pt1 = pow(v1_half, v1_half) / pow(v2_half, v2_half);
	double const_pt2 = pow(s1, v1_half) / pow(s2, v2_half);
	double const_pt3 = gsl_sf_gamma(v2_half) / gsl_sf_gamma(v1_half);
	double const_inv_gama = const_pt1 * const_pt2 * const_pt3;

	result = log(const_inv_gama) + (((double)amount_total) / 2.0) * ((variance / s1) - (gsl_sf_psi(v1_half) + log((2.0 / (v1 * s1)))));

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula aproximada.
// @param mean Média amostral do conjunto de valores reais.
// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
// @param amount_total Quantidade total de valores reais.
// @return Valor de surpresa.
double InvGammaDistribution::surpriseApproximation(double mean, double variance,
		int amount_total) {
	double result = 0.0;

	result = ((((double)amount_total) / 2.0) * ((variance / s1) + log((v1 * s1) / (2.0 * variance)) - gsl_sf_psi(v1 / 2.0)));

	return result;
}

} /* namespace dist */
