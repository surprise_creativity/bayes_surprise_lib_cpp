/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*               File: InvGammaDistribution.h   Modelo para dado real(mean fixa).    */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_MODEL_DISTRIBUTION_INVGAMMADISTRIBUTION_H_
#define SRC_SURP_MODEL_DISTRIBUTION_INVGAMMADISTRIBUTION_H_

#include "../DoubleModel.h"

namespace dist {

/**
 * Classe que implementa um Modelo Bayesiano para dados reais (apenas média fixa) com distribuição conjugada a priori Inversa Gamma Escalada.
 * Baseado na seção 3.4 do artigo "Of bits and wows: A Bayesian theory of surprise with applications to attention"
 * de Baldi, P. e Itti, L (2010).
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class InvGammaDistribution: public mdl::DoubleModel {
public:
	InvGammaDistribution();
	InvGammaDistribution(double _mean_data);
	InvGammaDistribution(double _v1, double _s1, double _mean_data);
	virtual ~InvGammaDistribution();

	void learn(double mean, double variance, int amount_total);
	void learn(std::vector<double> values);
	void learn(double value);

	double getSurprise(double mean, double variance, int amount_total);
	double getSurprise(std::vector<double> values);
	double getSurprise(double value);

	char getIdType();

	bool write(strm::BufferedOutputStream * bos);
	bool read(strm::BufferedInputStream * bis);

private:
	/**
	 * Parâmetros da distribuição (priori):
	 * primeiro parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 */
	double v1;

	/**
	 * Parâmetros da distribuição (priori):
	 * segundo parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 */
	double s1;

	/**
	 * Parâmetros da distribuição (likehood):
	 * média (fixa) das amostras de dados.
	 */
	double mean_data;

	double surprise(double mean, double variance, int amount_total);
	double surpriseApproximation(double mean, double variance, int amount_total);
};

} /* namespace dist */

#endif /* SRC_SURP_MODEL_DISTRIBUTION_INVGAMMADISTRIBUTION_H_ */
