/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: PoissonDistribution.cpp   Modelo para dado inteiro(>0).      */
/* --------------------------------------------------------------------------------- */

#include "PoissonDistribution.h"
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>
#include <cmath>

namespace dist {

/**
 * Construtor padrão.
 */
PoissonDistribution::PoissonDistribution() {
	a1 = 1;
	b1 = 1;
}

/**
 * Construtor para um valor de média a priori.
 * @param mean_priori Valor de média a priori.
 */
PoissonDistribution::PoissonDistribution(double mean_priori) {
	if(mean_priori < 0){
		mean_priori = 0;
	}
	if(mean_priori > 0){
		b1 = 1;
		a1 = mean_priori;
	}else if(mean_priori == 0){
		b1 = 1000000;
		a1 = 1;
	}
}

/**
 * Construtor para valores de parâmetros a priori passados.
 * @param _a1 Primeiro parâmetro da distribuição Poisson.
 * @param _b1 Segundo parâmetro da distribuição Poisson.
 */
PoissonDistribution::PoissonDistribution(double _a1, double _b1) {
	if(_a1 < 1){
		_a1 = 1;
	}
	if(_b1 < 1){
		_b1 = 1;
	}
	a1 = _a1;
	b1 = _b1;
}

PoissonDistribution::~PoissonDistribution() {

}

void PoissonDistribution::learn(double mean, int amount_total) {
	a1 = a1 + amount_total * mean;
	b1 = b1 + amount_total;
}

void PoissonDistribution::learn(std::vector<int> values) {
	double mean = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		int value = values[i];
		if(value > 0){
			mean += value;
		}
	}
	if(amount_total > 0){
		mean = mean / ((double)amount_total);
	}
	learn(mean, amount_total);
}

void PoissonDistribution::learn(int value) {
	double mean = 0.0;
	int amount_total = 1;
	if(value > 0){
		mean += value;
	}
	learn(mean, amount_total);
}

double PoissonDistribution::getSurprise(double mean, int amount_total) {
	double result = 0.0;

	if(amount_total > 0){
		// Versão aproximada:
		result = surpriseApproximation(mean, amount_total);
	}else{
		// Versão exata (Problemática para qualquer quantidade razoável de dados por causa da const_gamma):
		result = surprise(mean, amount_total);
	}

	return result;
}

double PoissonDistribution::getSurprise(std::vector<int> values) {
	double mean = 0.0;
	int amount_total = values.size();
	for(int i = 0;i < amount_total;i++){
		int value = values[i];
		if(value > 0){
			mean += value;
		}
	}
	if(amount_total > 0){
		mean = mean / ((double)amount_total);
	}
	return getSurprise(mean, amount_total);
}

double PoissonDistribution::getSurprise(int value) {
	double mean = 0.0;
	int amount_total = 1;
	if(value > 0){
		mean += value;
	}
	return getSurprise(mean, amount_total);
}

char PoissonDistribution::getIdType() {
	return 2;
}

bool PoissonDistribution::write(strm::BufferedOutputStream* bos) {
	bool result = true;

	bos->write(a1);
	bos->write(b1);
	bos->flush();

	return result;
}

bool PoissonDistribution::read(strm::BufferedInputStream* bis) {
	bool result = true;

	if(!bis->read(&a1)){
		result = false;
	}
	if(result && (!bis->read(&b1))){
		result = false;
	}

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula exata.
// @param mean Média amostral do conjunto de números inteiros (maiores que 0).
// @param amount_total Quantidade total de números inteiros (maiores que 0).
// @return Valor de surpresa.
double PoissonDistribution::surprise(double mean, int amount_total) {
	double result = 0.0;

	double b2 = (b1 + amount_total);
		double const_gamma = gsl_sf_gamma(a1 + amount_total * mean) / gsl_sf_gamma(a1);
	result = a1 * log(b1 / b2)
			 + log(const_gamma)
			 + amount_total * ((a1 / b1) + mean * (log(b1) - gsl_sf_psi(a1) - log(b2)));

	return result;
}

// Função que calcula surpresa bayesiana usando a fórmula aproximada.
// @param mean Média amostral do conjunto de números inteiros (maiores que 0).
// @param amount_total Quantidade total de números inteiros (maiores que 0).
// @return Valor de surpresa.
double PoissonDistribution::surpriseApproximation(double mean,
		int amount_total) {
	double result = 0.0;

	result = amount_total * ((a1 / b1) - mean * (1.0 - log(mean) + gsl_sf_psi(a1) - log(b1)));

	return result;
}

} /* namespace dist */
