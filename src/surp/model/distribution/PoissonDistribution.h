/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: PoissonDistribution.h   Modelo para dado inteiro(>0).        */
/* --------------------------------------------------------------------------------- */

#ifndef SRC_SURP_MODEL_DISTRIBUTION_POISSONDISTRIBUTION_H_
#define SRC_SURP_MODEL_DISTRIBUTION_POISSONDISTRIBUTION_H_

#include "../IntegerModel.h"

namespace dist {

/**
 * Classe que implementa um Modelo Bayesiano para dados inteiros (maiores que 0) com distribuição conjugada a priori Poisson.
 * Baseado na seção 3.2 do artigo "Of bits and wows: A Bayesian theory of surprise with applications to attention"
 * de Baldi, P. e Itti, L (2010).
 *
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

class PoissonDistribution: public mdl::IntegerModel {
public:
	PoissonDistribution();
	PoissonDistribution(double mean_priori);
	PoissonDistribution(double _a1, double _b1);
	virtual ~PoissonDistribution();

	void learn(double mean, int amount_total);
	void learn(std::vector<int> values);
	void learn(int value);

	double getSurprise(double mean, int amount_total);
	double getSurprise(std::vector<int> values);
	double getSurprise(int value);

	char getIdType();

	bool write(strm::BufferedOutputStream * bos);
	bool read(strm::BufferedInputStream * bis);

private:
	/**
	 * Parâmetros da distribuição (priori):
	 * primeiro parâmetro da distribuição Poisson.
	 */
	double a1;

	/**
	 * Parâmetros da distribuição (priori):
	 * segundo parâmetro da distribuição Poisson.
	 */
	double b1;

	double surprise(double mean, int amount_total);
	double surpriseApproximation(double mean, int amount_total);
};

} /* namespace dist */

#endif /* SRC_SURP_MODEL_DISTRIBUTION_POISSONDISTRIBUTION_H_ */
